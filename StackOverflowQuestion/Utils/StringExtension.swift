//
//  StringExtension.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 14.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation

extension String {
    
    func mySubstring(with nsrange: NSRange) -> Substring? {
        guard let range = Range(nsrange, in: self) else { return nil }
        return self[range]
    }
}
