//
//  stringHtmlParsingExtension.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 10.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation
import Kanna

extension String{
    func parsingHtml() -> NSAttributedString{
        if let doc = try? HTML(html: self, encoding: .utf8){
            var str = self
            str = str.replacingOccurrences(of: "<pre>", with: "\n", options: .literal, range: nil)
            str = str.replacingOccurrences(of: "</pre>", with: "\n", options: .literal, range: nil)
            var str2 = str
            let tagPattern = "(<[a-z0-9\\/]+>)"
            let regExTag = RegEx(_pattern: tagPattern)
            let tagsRange = regExTag.Match(input: str)
            for tagRang in tagsRange!{
                print(str2.mySubstring(with: tagRang) ?? "")
                str = str.replacingOccurrences(of: str2.mySubstring(with: tagRang) ?? "", with: " ", options: .literal, range: nil)
            }

            let attributedString = NSMutableAttributedString(string: str)
            for code in doc.xpath("code | //code"){
                let range = (str as NSString).range(of: code.text!, options: NSString.CompareOptions.diacriticInsensitive)
                attributedString.addAttributes([NSAttributedStringKey.backgroundColor : UIColor.gray, NSAttributedStringKey.font : UIFont(name: "Menlo-Italic", size: 18)!], range: range)
            }
            return attributedString
        }
        return NSAttributedString(string: self)
    }
}
