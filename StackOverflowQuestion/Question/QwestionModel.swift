//
//  QwestionModel.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 03.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation

class QwestionModel{
    //MARK: propertys
    static let shared = QwestionModel()
    let downloadQwestionService = DownloadQwestionService.shared
    //MARK: methods
    func getQwestionData(forTag tag: String, page: Int, completionHandler: @escaping qwestionsDataCompletion){
        downloadQwestionService.getQwestionsData(forTag: tag, page: page){ qwestionData, error in
            if error != nil{
                completionHandler(nil,error)
                return
            }
            completionHandler(qwestionData, error)
        }
    }
}
