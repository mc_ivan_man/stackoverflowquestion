//
//  PreparedDataForCellDisplay.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 09.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation

struct PreparedDataForCellDisplay {
    var text: NSMutableAttributedString
    let lastActivityDate: Double
    let displayName: String
    let score: Int
    let isAccepted: Bool
}
