//
//  AnswerViewController.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 03.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import UIKit

class AnswerViewController: UIViewController, UITableViewDataSource {
    
    //MARK: Property
    @IBOutlet weak var answerTableView: UITableView!
    var qwestion:Discussion?
    var answers = [Answer]()
    let answerModel = AnswerModel.shared
    var loading = true
    var refreshControl: UIRefreshControl!
    
    //MARK: ViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        answerTableView.addSubview(refreshControl)
        let nib = UINib(nibName: "LoadingCell", bundle: Bundle.main)
        answerTableView.register(nib, forCellReuseIdentifier: "LoadingCell")
        answerTableView.reloadData()
        downloadAnswersData(forQwestion: qwestion!)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: UITableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if loading{
            return 1
        }
        else{
            return answers.count+1
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if loading{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as! LoadingCell
            cell.loadingIndicator.startAnimating()
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "AnswerCell") as! AnswerTableViewCell
            if indexPath.row == 0{
                print("вопрос:\(qwestion!.body)")
                cell.mapingDiscussionData(data: qwestion!)
            }
            else{
                print("ответ\(indexPath.row - 1):\(answers[indexPath.row - 1].body)")
                cell.mapingAnswerData(data: answers[indexPath.row - 1])
            }
            return cell
        }
    }
    
    //MARK: methods
    @objc func refresh(_ sender: Any) {
        downloadAnswersData(forQwestion: qwestion!)
    }
    
    func loadAlert(title:String, message: String, buttonTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    func downloadAnswersData(forQwestion qwestion: Discussion){
        answerModel.getData(forQwestion: qwestion){
            data, error in
            if error == nil{
                print("ответов загружено: \(data!.count)")
                self.answers = data!
                self.refreshControl.endRefreshing()
                self.loading = false
                self.answerTableView.reloadData()
            }
            else{
                switch error!{
                case DownloadError.invalidURL:
                    self.loadAlert(title: "Ошибка HTTP запроса", message: "Нажмите Назад и повторите попытку", buttonTitle: "Назад")
                case DownloadError.invalidResponse:
                    self.loadAlert(title: "Ошибка HTTP запроса", message: "Нажмите Назад и повторите попытку", buttonTitle: "Назад")
                case DownloadError.notInternetConection:
                    self.loadAlert(title: "Отсутствует соединение с интернетом", message: "Проверте интернет соединение и повторите попытку", buttonTitle: "Назад")
                default:
                    let error = error as NSError?
                    print(error!.code)
                    self.loadAlert(title: "Ошибка", message: "Ошибка:\(error!.localizedDescription) \nНажмите Назад для повторного ввода", buttonTitle: "Назад")
                }
            }
        }
    }

}
