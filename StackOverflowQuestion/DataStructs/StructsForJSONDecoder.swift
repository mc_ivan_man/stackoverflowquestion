//
//  StructsForJSONDecoder.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 03.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation

struct Discussions:Codable{
    let items:[Discussion]
}

struct Discussion: Codable{
    let id: Int
    let owner: Owner
    let title: String
    let answerCount: Int
    let lastActivityDate: Double
    let score: Int
    let body: String
    
    private enum CodingKeys: String, CodingKey {
        case id = "question_id"
        case owner = "owner"
        case title = "title"
        case answerCount = "answer_count"
        case lastActivityDate = "last_activity_date"
        case score = "score"
        case body = "body"
    }
}

struct Owner: Codable{
    let displayName: String
    
    private enum CodingKeys: String, CodingKey {
        case displayName = "display_name"
    }
}

struct Answers:Codable{
    let items:[Answer]
}

struct Answer: Codable{
    let id: Int
    let owner: Owner
    let score: Int
    let isAccepted: Bool
    let lastActivityDate: Double
    let body: String
    
    private enum CodingKeys: String, CodingKey {
        case id = "answer_id"
        case owner = "owner"
        case score = "score"
        case isAccepted = "is_accepted"
        case lastActivityDate = "last_activity_date"
        case body = "body"
    }
}
