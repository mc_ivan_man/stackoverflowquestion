//
//  QwestionTableViewCell.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 03.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import UIKit

class QwestionTableViewCell: UITableViewCell {
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var title: UILabel!
    @IBOutlet weak var answerCount: UILabel!
    @IBOutlet weak var date: UILabel!
    var qwestion:Discussion? 
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }
    
    
    
    func mapingCellData(qwestion: Discussion){
        author.text = qwestion.owner.displayName
        title.text = qwestion.title
        answerCount.text = "Ответов: \(qwestion.answerCount)"
        date.text = qwestion.lastActivityDate.getDateStringFromUTC()
        self.qwestion = qwestion
    }

}
