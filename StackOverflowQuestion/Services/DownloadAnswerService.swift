//
//  DownloadAnswerService.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 03.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation
typealias answersDataCompletion = ([Answer]?,Error?) -> Void

class DownloadAnswerService{
    
    static let shared = DownloadAnswerService()
    
    func getAnswersData(forQwestionID id: Int, completionHandler: @escaping answersDataCompletion) {
        guard let url = URL(string: "https://api.stackexchange.com/2.2/questions/\(id)/answers?order=desc&filter=withbody&sort=activity&site=stackoverflow") else {
            completionHandler(nil,DownloadError.invalidURL)
            return
        }
        let session = URLSession(configuration: .default)
        let downloadTask = session.dataTask(with: url) { (data, response, error) in
            if error != nil{
                let error = error as NSError?
                if error?.code == -1009{
                    OperationQueue.main.addOperation {
                        completionHandler(nil, DownloadError.notInternetConection)
                    }
                    return
                }
                else{
                    OperationQueue.main.addOperation {
                        completionHandler(nil, error)
                    }
                    return
                }
            }
            guard let data = data else {return}
            do{
                let decoder = JSONDecoder()
                let answers = try decoder.decode(Answers.self, from: data)
                OperationQueue.main.addOperation {
                    completionHandler(answers.items, nil)
                }
                return
            }catch{
                completionHandler(nil, DownloadError.invalidResponse)
            }
        }
        downloadTask.resume()
    }
}

