//
//  AnswerModel.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 03.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation


class AnswerModel{
    //MARK: propertys
    static let shared = AnswerModel()
    
    let downloadAnswerService = DownloadAnswerService.shared
    //MARK: methods
    func getData(forQwestion qwestion: Discussion, completionHandler: @escaping answersDataCompletion){
        downloadAnswerService.getAnswersData(forQwestionID: qwestion.id){
            answers, error in
            completionHandler(answers,error)
        }
    }
}
