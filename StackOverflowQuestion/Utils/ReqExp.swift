//
//  ReqExp.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 14.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation

struct RegEx{
    let regex:NSRegularExpression?
    
    init (_pattern: String){
        self.regex = try? NSRegularExpression(pattern: _pattern, options: .caseInsensitive)
    }
    
    func Match(input: String)->[NSRange]?{
        if let matches = regex?.matches(in: input, options: .reportCompletion, range: NSMakeRange(0, input.count)){
            if matches.count == 0{
                return nil
            }
            var ranges = [NSRange]()
            for match in matches{
                ranges.append(match.range)
            }
            return ranges
        }
        else{
            return nil
        }
    }
}
