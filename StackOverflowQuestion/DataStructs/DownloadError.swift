//
//  DownloadError.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 03.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation

enum DownloadError: Error{
    case notInternetConection
    case invalidResponse
    case invalidURL
    case notData
}
