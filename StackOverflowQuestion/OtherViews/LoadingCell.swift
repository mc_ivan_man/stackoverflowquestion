//
//  LoadingCell.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 09.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import UIKit

class LoadingCell: UITableViewCell {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
