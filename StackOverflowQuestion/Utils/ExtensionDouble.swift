//
//  extensionDouble.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 04.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation

extension Double{
    func getDateStringFromUTC()-> String{
        let date = Date(timeIntervalSince1970: self)
        
        let currentDate = Date()
        let timeInterval = currentDate.timeIntervalSince(date)
        
        if timeInterval < 60 {
            return "modified " + Int(timeInterval).description + " secs ago"
        } else if timeInterval >= 60 && timeInterval < 3600 {
            return "modified " + Int(timeInterval/60).description + " mins ago"
        } else if timeInterval >= 3600 && timeInterval < 3600 * 24 {
            return "modified " + Int(timeInterval/3600).description + " hours ago"
        } else {
            let dateFormatter = DateFormatter()
            dateFormatter.dateStyle = .medium
            dateFormatter.timeStyle = .short
            dateFormatter.locale = Locale(identifier: "en_US")
            let dateString = dateFormatter.string(from: date)
            return dateString
        }
    }
}
