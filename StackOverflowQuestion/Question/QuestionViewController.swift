//
//  QuestionViewController.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 03.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import UIKit

class QuestionViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UIScrollViewDelegate{
    
    //MARK: Property
    let myPicker = UIPickerView()
    let tags = ["objective-c","ios","xcode","cocoa-touch","iphone"]
    @IBOutlet weak var qwestionTableView: UITableView!
    var qwestionData = [Discussion]()
    let qwestionModel = QwestionModel.shared
    var tag: String = ""{
        didSet{
            self.qwestionTableView.setContentOffset(.zero, animated: true)
            qwestionTableView.reloadData()
            self.navigationItem.title = "Вопросы по тегу: \(tag)"
            page = 1
            downloadQwestiodData()
        }
    }
    var refreshControl: UIRefreshControl!
    var loading = true
    var page = 1
    
    //MARK: ViewController methods
    override func viewDidLoad() {
        super.viewDidLoad()
        refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: #selector(refresh), for: .valueChanged)
        qwestionTableView.addSubview(refreshControl)
        let nib = UINib(nibName: "LoadingCell", bundle: Bundle.main)
        qwestionTableView.register(nib, forCellReuseIdentifier: "LoadingCell")
        qwestionTableView.reloadData()
        tag = "objective-c"
        loading = true
        myPicker.delegate = self
        self.myPicker.frame = qwestionTableView.frame
        self.myPicker.backgroundColor = UIColor.gray.withAlphaComponent(0.9)
        self.view.addSubview(myPicker)
        self.view.sendSubview(toBack: myPicker)
        
    }
    
    //MARK: UITableView methods
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if loading{
            return 1
        }
        else{
            return qwestionData.count
        }
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        if loading{
            let cell = tableView.dequeueReusableCell(withIdentifier: "LoadingCell", for: indexPath) as! LoadingCell
            cell.loadingIndicator.startAnimating()
            return cell
        }
        else{
            let cell = tableView.dequeueReusableCell(withIdentifier: "QwestionCell") as! QwestionTableViewCell
            cell.mapingCellData(qwestion: qwestionData[indexPath.row])
            return cell
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let cell = tableView.cellForRow(at: indexPath) as! QwestionTableViewCell
        let viewController = self.storyboard?.instantiateViewController(withIdentifier: "AnswerViewController") as! AnswerViewController
        viewController.qwestion = cell.qwestion
        self.navigationController!.pushViewController(viewController, animated: true)
    }
    
    //MARK: UIPikerView methods
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return tags.count
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return tags[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        tag = tags[row]
        self.view.sendSubview(toBack: pickerView)
        loading = true
        qwestionTableView.reloadData()
    }
    
    //MARK: UIScrolViewDelegate methods and property
    let threshold: CGFloat = 100.0
    var isLoadingMore = false
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        let contentOffset = scrollView.contentOffset.y
        let maximumOffset = scrollView.contentSize.height - scrollView.frame.size.height;
        
        if !isLoadingMore && (maximumOffset - contentOffset <= threshold) {
            self.isLoadingMore = true
            // Update UI
            pageNewQwestionData()
        }
    }
    
    //MARK: methods
    @objc func refresh(_ sender: Any) {
        let str = tag
        tag = str
    }
    
    func pageNewQwestionData(){
        page += 1
        qwestionModel.getQwestionData(forTag: tag, page: page){
            qwestData, error in
            if error == nil{
                print("вопросов догружено: \(qwestData!.count)")
                self.qwestionData += qwestData!
                self.qwestionTableView.beginUpdates()
                self.qwestionTableView.insertRows(at: self.createIndexesToPageNewQwestionData(), with: .automatic)
                self.qwestionTableView.endUpdates()
                self.isLoadingMore = false
            }
            else{
                self.errorAlert(error: error!)
            }
        }
    }
    
    func createIndexesToPageNewQwestionData() ->[IndexPath]{
        var res = [IndexPath]()
        for i in 0...19{
            res.append(IndexPath(row: self.qwestionData.count + i - 21, section: 0))
        }
        return res
    }
    
    func downloadQwestiodData(){
        qwestionModel.getQwestionData(forTag: tag, page: page){
            qwestData, error in
            if error == nil{
                print("вопросов загружено: \(qwestData!.count)")
                self.refreshControl.endRefreshing()
                self.loading = false
                self.qwestionData = qwestData!
                self.qwestionTableView.reloadData()
            }
            else{
                self.errorAlert(error: error!)
            }
        }
    }
    
    func errorAlert(error: Error){
        switch error{
        case DownloadError.invalidURL:
            self.loadAlert(title: "Ошибка HTTP запроса", message: "Нажмите Назад и повторите попытку", buttonTitle: "Назад")
        case DownloadError.invalidResponse:
            self.loadAlert(title: "Ошибка HTTP запроса", message: "Нажмите Назад и повторите попытку", buttonTitle: "Назад")
        case DownloadError.notInternetConection:
            self.loadAlert(title: "Отсутствует соединение с интернетом", message: "Проверте интернет соединение и повторите попытку", buttonTitle: "Назад")
        default:
            self.loadAlert(title: "Ошибка", message: "Ошибка:\(error.localizedDescription) \nНажмите Назад для повторного ввода", buttonTitle: "Назад")
        }
    }
    
    func loadAlert(title:String, message: String, buttonTitle: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let action = UIAlertAction(title: buttonTitle, style: .default, handler: nil)
        alert.addAction(action)
        present(alert, animated: true, completion: nil)
    }
    
    @IBAction func changeTag(_ sender: Any) {
        
        self.myPicker.frame = qwestionTableView.frame
        self.view.bringSubview(toFront: myPicker)
    }
}

