//
//  AnswerTableViewCell.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 08.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import UIKit

class AnswerTableViewCell: UITableViewCell {
    //MARK: propertys
    @IBOutlet weak var author: UILabel!
    @IBOutlet weak var isAccepted: UIImageView!
    @IBOutlet weak var answerText: UILabel!
    @IBOutlet weak var score: UILabel!
    @IBOutlet weak var lastActivityDate: UILabel!
    
    //MARK: methods
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func mapingDiscussionData(data: Discussion){
        self.backgroundColor = .lightGray
        author.text = data.owner.displayName
        answerText.attributedText = data.body.parsingHtml()
        score.text = "рейтинг:\(data.score)"
        lastActivityDate.text = data.lastActivityDate.getDateStringFromUTC()
    }
    
    func mapingAnswerData(data: Answer){
        self.backgroundColor = .white
        author.text = data.owner.displayName
        let image:UIImage?
        if data.isAccepted {
            image = UIImage(named: "ok")
        }
        else{
            image = UIImage(named: "no")
        }
        isAccepted.image = image
        answerText.attributedText = data.body.parsingHtml()
        score.text = "рейтинг:\(data.score)"
        lastActivityDate.text = data.lastActivityDate.getDateStringFromUTC()
    }
}
