//
//  DownloadQwestionService.swift
//  StackOverflowQuestion
//
//  Created by Иван Лебедев on 03.10.2018.
//  Copyright © 2018 IvanLebedev. All rights reserved.
//

import Foundation
typealias qwestionsDataCompletion = ([Discussion]?,Error?) -> Void
typealias qwestionDataCompletion = (Discussion?,Error?) -> Void

class DownloadQwestionService{
    
    static let shared = DownloadQwestionService()
    
    func getQwestionsData(forTag tag: String, page: Int, completionHandler: @escaping qwestionsDataCompletion) {
        guard let url = URL(string: "https://api.stackexchange.com/2.2/questions?order=desc&page=\(page)&pagesize=20&sort=creation&filter=withbody&tagged=\(tag)&site=stackoverflow") else {
            completionHandler(nil,DownloadError.invalidURL)
            return
        }
        let session = URLSession(configuration: .default)
        let downloadTask = session.dataTask(with: url) { (data, response, error) in
            if error != nil{
                let error = error as NSError?
                if error?.code == -1009{
                    OperationQueue.main.addOperation {
                        completionHandler(nil, DownloadError.notInternetConection)
                    }
                    return
                }
                else{
                    OperationQueue.main.addOperation {
                        completionHandler(nil, error)
                    }
                    return
                }
            }
            guard let data = data else {return}
            do{
                let decoder = JSONDecoder()
                let dicsussions = try decoder.decode(Discussions.self, from: data)
                OperationQueue.main.addOperation {
                    completionHandler(dicsussions.items, nil)
                }
                return
            }catch{
                completionHandler(nil, DownloadError.invalidResponse)
            }
        }
        downloadTask.resume()
    }
    
    
    }
